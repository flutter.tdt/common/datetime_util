library datetime_util;
import 'dart:core';
import 'package:intl/intl.dart';
/// A DateTime Utility.
class DateUtil {
  static DateTime parse(String value, String format) {
    return DateFormat(format).parse(value);
  }

  /// Returns after n working days of datetime
  static DateTime addWorkingDay(DateTime datetime, int nday) => datetime.add(Duration(days: nday));
}